package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {

	@Test
	public void testFrameFirstThrow() throws BowlingException {
		Game game = new Game();
		
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
	
		assertEquals(7, game.getFrameAt(2).getFirstThrow());
	}
	
	@Test
	public void testFrameSecondThrow() throws BowlingException {
		Game game = new Game();
		
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
	
		assertEquals(2, game.getFrameAt(2).getSecondThrow());
	}
	
	@Test
	public void testGameScore() throws BowlingException {
		Game game = new Game();
		
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
	
		assertEquals(81, game.calculateScore());
	}
	
	@Test
	public void testGameScoreWithBonus() throws BowlingException {
		Game game = new Game();
		
		game.addFrame(new Frame(1, 9));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
	
		assertEquals(88, game.calculateScore());
	}

	@Test
	public void testGameScoreWithStrike() throws BowlingException {
		Game game = new Game();
		
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
	
		assertEquals(94, game.calculateScore());
	}
	
	@Test
	public void testGameScoreWithStrikeAndSpare() throws BowlingException {
		Game game = new Game();
		
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(4, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
	
		assertEquals(103, game.calculateScore());
	}
	
	@Test
	public void testGameScoreWithMultipleStrikes() throws BowlingException {
		Game game = new Game();
		
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
	
		assertEquals(112, game.calculateScore());
	}
	
	@Test
	public void testGameScoreWithMultipleSpares() throws BowlingException {
		Game game = new Game();
		
		game.addFrame(new Frame(8, 2));
		game.addFrame(new Frame(5, 5));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
	
		assertEquals(98, game.calculateScore());
	}
	
	@Test
	public void testFirstBonusThrow() throws BowlingException {
		Game game = new Game();
		game.setFirstBonusThrow(7);
	
		assertEquals(7, game.getFirstBonusThrow());
	}
	
	@Test
	public void testGameScoreWithBonusThrow() throws BowlingException {
		Game game = new Game();
		
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 8));
	
		game.setFirstBonusThrow(7);
		
		assertEquals(90, game.calculateScore());
	}
	
	@Test
	public void testSecondBonusThrow() throws BowlingException {
		Game game = new Game();
		game.setSecondBonusThrow(2);
	
		assertEquals(2, game.getSecondBonusThrow());
	}
	
	@Test
	public void testGameScoreWithTwoBonusThrow() throws BowlingException {
		Game game = new Game();
		
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(10 ,0));
	
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		
		assertEquals(92, game.calculateScore());
	}
	
	@Test
	public void testGameBestScore() throws BowlingException {
		Game game = new Game();
		
		game.addFrame(new Frame(10 ,0));
		game.addFrame(new Frame(10 ,0));
		game.addFrame(new Frame(10 ,0));
		game.addFrame(new Frame(10 ,0));
		game.addFrame(new Frame(10 ,0));
		game.addFrame(new Frame(10 ,0));
		game.addFrame(new Frame(10 ,0));
		game.addFrame(new Frame(10 ,0));
		game.addFrame(new Frame(10 ,0));
		game.addFrame(new Frame(10 ,0));
	
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		
		assertEquals(300, game.calculateScore());
	}
	
}
