package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.Frame;


public class FrameTest {

	@Test
	public void testFirstThrow() throws Exception{
		Frame frame = new Frame(2, 4);
		assertEquals(2, frame.getFirstThrow());
	}
	
	@Test
	public void testSecondThrow() throws Exception{
		Frame frame = new Frame(2, 4);
		assertEquals(4, frame.getSecondThrow());
	}
	
	@Test
	public void testScore() throws Exception{
		Frame frame = new Frame(2, 6);
		assertEquals(8, frame.getScore());
	}
	
	@Test
	public void testBonus() throws Exception{
		Frame frame = new Frame(2, 6);
		frame.setBonus(3);
		
		assertEquals(3, frame.getBonus());
	}
	
	@Test
	public void testSpare() throws Exception{
		Frame frame = new Frame(4, 6);
		assertTrue(frame.isSpare());
	}
	
	@Test
	public void testStrike() throws Exception{
		Frame frame = new Frame(10, 0);
		assertTrue(frame.isStrike());
	}
	
}
